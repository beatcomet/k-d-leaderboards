package me.beatcomet.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;
import me.anni.LeaderBoards.LeaderBoards;

public class Config {
	
	File config;
	LeaderBoards plugin;
	
	public Config(File config, LeaderBoards instance){
		this.config = config;
		this.plugin = instance;
	}
	
	public String getString(String key, String defvalue){
		if(!config.exists()){
			try {
				config.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		HashMap<String, String> data = this.getData();
		if(data.containsKey(key)){
			return data.get(key);
		}
		else{
			this.addData(key, defvalue);
			return defvalue;
		}
	}
	
	public HashMap<String, String> getData(){
		HashMap<String, String> data = new HashMap<String, String>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(config);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		while(scanner.hasNextLine()){
			String[] line = scanner.nextLine().split(": ");
			if(line.length == 1)
				data.put(line[0], "");
			else
				data.put(line[0], line[1]);
		}
		scanner.close();
		return data;
	}
	
	public void addData(String key, String val){
		HashMap<String, String> data = this.getData();
		data.put(key, val);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(config);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(String s : data.keySet()){
			writer.printf("%s: %s\n", s, data.get(s));
		}
		writer.close();
	}
	
	

}
