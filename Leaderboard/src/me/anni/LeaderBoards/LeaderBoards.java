package me.anni.LeaderBoards;

import java.io.File;
import java.util.logging.Logger;
import lib.PatPeter.SQLibrary.MySQL;
import org.bukkit.plugin.java.JavaPlugin;
import me.beatcomet.config.*;

public class LeaderBoards extends JavaPlugin {

	public static LeaderBoards plugin;
	public final Logger logger = Logger.getLogger("Minecraft");;
	public String logPrefix = "[LB] ";
	protected SQL data;

	private String url, user, password, database, port, driver;
	public String rankby;

	public final LeaderBoardsPlayerListener playerListener = new LeaderBoardsPlayerListener(
			this);

	@Override
	public void onDisable() {
		data.connect(false, driver);
	}

	@Override
	public void onEnable() {
		/*
		 * Implement way to choose sort method, either by kills or kdr
		 */
		//rankby = this.getConfig().getString("SortBy", "kills");
		Logger.getLogger("Minecraft");
		
		this.getDataFolder().mkdirs();
		File configfile = new File(this.getDataFolder().getAbsolutePath() + "/config.ini");
		Config config = new Config(configfile, this);
		
		url = config.getString("host", "localhost");
		user = config.getString("user", "root");
		password = config.getString("pass", "");
		database = config.getString("database", "kblb");
		port = config.getString("port", "3306");
		rankby = config.getString("SortBy", "kills");
		driver = config.getString("driver", "sqlite");
		
		
		//Added the new configuration system above :)
		/*
		url = this.getConfig().getString("mysql.host", "localhost");
		user = this.getConfig().getString("mysql.user", "root");
		password = this.getConfig().getString("mysql.pass", "");
		database = this.getConfig().getString("mysql.database", "kblb");
		port = this.getConfig().getString("mysql.port", "3306");
		*/

		

		this.getServer().getPluginManager()
		.registerEvents(playerListener, this);

		getCommand("kd").setExecutor(new KdrExecutor(this));
		getCommand("topKills").setExecutor(new TopKillsExecutor(this));

		// Feel free to change the commands if you think of a better command
		// (like kdrank or something)
		getCommand("kdrrank").setExecutor(new RankExecutor(this));
		
		data = new SQL(url, database, user, password, port, "kblb", this);
		if(!data.connect(true, driver)){
			this.getServer().getPluginManager().disablePlugin(this);
		}
	}

	public MySQL getDriver() {
		return data.getDriver();
	}

}
