package me.anni.LeaderBoards;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RankExecutor implements CommandExecutor {

	private LeaderBoards plugin;

	private ChatColor RED = ChatColor.RED;
	private ChatColor WHITE = ChatColor.WHITE;

	public RankExecutor(LeaderBoards plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		String sortmethod = plugin.rankby;
		String name = "";
		int rank = 0;
		if(label.equalsIgnoreCase("kdrrank")){
			if(sender.hasPermission("lb.rank")){
				if (plugin.getDriver().query("SELECT * FROM kblb") != null) {
					if(args.length != 0){
						name = args[0];
					}
					else{
						name = sender.getName();
					}
					try {
						rank = plugin.data.getRank(name, sortmethod);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					if(rank != 0){
						double kdr = 0;
						int kills = 0;
						int deaths = 0;
						try {
							kdr = plugin.data.calculateKDR(name);
							kills = plugin.data.getKills(name);
							deaths = plugin.data.getDeaths(name);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						sender.sendMessage(WHITE + name + RED + " Rank: " + WHITE
								+ rank + RED + " Kills: " + WHITE + kills + RED
								+ " Deaths: " + WHITE + deaths + RED + " K/D: "
								+ WHITE + kdr);

					}
				}
				return true;
			}
		}
		/*
		if (label.equalsIgnoreCase("kdrrank")) {
			if (sender.hasPermission("lb.rank")) {
				if (plugin.getDriver().query("SELECT * FROM kblb") != null) {
					String p = null;
					int rank = 0;
					if (args.length != 0) {
						try {
							p = args[0];
							rank = plugin.data.getRankKills(p);
						} catch (SQLException e) {
							plugin.logger
									.warning(plugin.logPrefix
											+ "Error at line 34 Class:RankExecutor.java"
											+ e.getMessage());
						}
					} else {
						try {
							p = sender.getName();
							rank = plugin.data.getRankKills(p);
						} catch (SQLException e) {
							plugin.logger
									.warning(plugin.logPrefix
											+ "Error at line 41 Class:rankKillsExecutor.java"
											+ e.getMessage());
						}
					}
					if (rank != 0) {
						double kdr = 0;
						int kills = 0;
						int deaths = 0;
						try {
							kdr = plugin.data.calculateKDR(p);
							kills = plugin.data.getKills(p);
							deaths = plugin.data.getDeaths(p);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						sender.sendMessage(WHITE + p + RED + " Rank: " + WHITE
								+ rank + RED + " Kills: " + WHITE + kills + RED
								+ " Deaths: " + WHITE + deaths + RED + " K/D: "
								+ WHITE + kdr);

					}
					return true;
				}
				sender.sendMessage(RED + "No data found!");
				return true;
			}
		}*/
		return false;
	}

}
