package me.anni.LeaderBoards;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TopKillsExecutor implements CommandExecutor {

	private LeaderBoards plugin;

	private ChatColor RED = ChatColor.RED;
	private ChatColor WHITE = ChatColor.WHITE;

	public TopKillsExecutor(LeaderBoards plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (label.equalsIgnoreCase("topkills")) {
			if (sender.hasPermission("lb.top")) {
				if (plugin.getDriver().query("SELECT * FROM kblb") != null) {

					int count = Integer.parseInt(args[0]);
					String[] top = null;
					if (args.length != 0) {
						try {
							top = plugin.data.getTopKills(count);
						} catch (SQLException e) {
							plugin.logger
							.warning(plugin.logPrefix
									+ "Error at line 34 Class:TopKillsExecutor.java"
									+ e.getMessage());
						}
					} else {
						try {
							top = plugin.data.getTopKills(10);
						} catch (SQLException e) {
							plugin.logger
							.warning(plugin.logPrefix
									+ "Error at line 41 Class:TopKillsExecutor.java"
									+ e.getMessage());
						}
					}
					if (top != null) {
						for (int i = 0; i < top.length; i++) {
							double kdr = 0;
							int kills = 0;
							int deaths = 0;
							try {
								kdr = plugin.data.calculateKDR(top[i]);
								kills = plugin.data.getKills(top[i]);
								deaths = plugin.data.getDeaths(top[i]);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							if (kdr == -1) {
								sender.sendMessage(RED + "" + i + 1 + ". "
										+ WHITE + top[i] + RED + " Kills: "
										+ WHITE + kills + RED + " Deaths: "
										+ WHITE + deaths + RED + " K/D: "
										+ WHITE + "infinite");
							}
							sender.sendMessage(RED + "" + i + 1 + ". " + WHITE
									+ top[i] + RED + " Kills: " + WHITE + kills
									+ RED + " Deaths: " + WHITE + deaths + RED
									+ " K/D: " + WHITE + kdr);
						}

					}
					return true;
				}
				sender.sendMessage(RED + "No data found!");
				return true;
			}
		}
		return false;
	}

}
