package me.anni.LeaderBoards;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class KdrExecutor implements CommandExecutor {

	private LeaderBoards plugin;

	private ChatColor RED = ChatColor.RED;
	private ChatColor WHITE = ChatColor.WHITE;

	public KdrExecutor(LeaderBoards plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (label.equalsIgnoreCase("kd")) {
			int kills = 0, deaths = 0;
			double kdr = 0.0;
			if (sender.hasPermission("lb.kd")) {
				if (args.length != 0) {
					if (plugin.getDriver().query(
							"SELECT * FROM kblb WHERE PlayerName='"
									+ args[0].toString().toLowerCase() + "'") != null) {
						try {
							kills = plugin.data.getKills(args[0]);
							deaths = plugin.data.getDeaths(args[0]);
							kdr = plugin.data.calculateKDR(kills, deaths);
						} catch (Exception e) {
							plugin.logger
							.info(plugin.logPrefix
									+ "Error at line 30 Class:KdrExecutor.java "
									+ e.getMessage());
						}

						sender.sendMessage(WHITE
								+ args[0].toString().toLowerCase() + RED
								+ " Kills: " + WHITE + kills + RED
								+ " Deaths: " + WHITE + deaths + RED + " K/D: "
								+ WHITE + kdr);
					} else {
						sender.sendMessage(WHITE + args[0] + RED
								+ " is not a valid player name.");
					}
					return true;
				}
				try {
					kills = plugin.data.getKills(sender.getName());
					deaths = plugin.data.getDeaths(sender.getName());
					kdr = plugin.data.calculateKDR(kills, deaths);
				} catch (Exception e) {
					plugin.logger.info(plugin.logPrefix
							+ "Error at line 30 Class:KdrExecutor.java "
							+ e.getMessage());
				}
				sender.sendMessage(WHITE + sender.getName().toLowerCase()
						+ RED + " Kills: " + WHITE + kills + RED + " Deaths: "
						+ WHITE + deaths + RED + " K/D: " + WHITE + kdr);
				return true;
			}
		}
		return false;
	}

}
