package me.anni.LeaderBoards;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import lib.PatPeter.SQLibrary.MySQL;
import lib.PatPeter.SQLibrary.SQLite;

public class SQL {

	private String url, db, user, pass, port, table;
	private MySQL driver;
	private SQLite driver2;
	private LeaderBoards plugin;

	public SQL(String url, String db, String user, String pass, String port,
			String table, LeaderBoards instance) {
		this.url = url;
		this.db = db;
		this.user = user;
		this.pass = pass;
		this.port = port;
		this.table = table;
		this.plugin = instance;
	}

	//The new connect method! it's better than the old one which was stupid and useless
	
	public boolean connect(boolean b, String drv) {
		Logger log = plugin.logger;
		if(drv.equalsIgnoreCase("mysql"))
		{
			driver = new MySQL(log, plugin.logPrefix, this.url, this.port, this.db, 
					this.user, this.pass);
			if(b == true){
				if(driver.open() != null)
				{
					log.info(plugin.logPrefix + "Established connection to MySQL!");
					log.info(plugin.logPrefix + "Looking for data...");
					if (!driver.checkTable(table)) {
						log.info(plugin.logPrefix
								+ "No data found, creating table now!");
						this.createTable(driver);
						log.info(plugin.logPrefix + "Finished creating a table");
					}
					log.info(plugin.logPrefix + "Table found!");
					return true;
				}
				else{
					log.info(plugin.logPrefix
							+ "Connection to MySQL could not be established, Unloading the plugin!");
					// Disable the plugin, since there is no connection to a database
					return false;
				}
			}
			else{
				driver.close();
				return true;
			}
		}
		else if(drv.equalsIgnoreCase("sqlite")){
			driver2 = new SQLite(log, plugin.logPrefix, "kblb", plugin.getDataFolder().getAbsolutePath());
			if(b == true){
				log.info(plugin.logPrefix + "Established connection to an SQLite database!");
				log.info(plugin.logPrefix + "Looking for data...");
				if (!driver2.checkTable(table)) {
					log.info(plugin.logPrefix
							+ "No data found, creating table now!");
					this.createTable(driver2);
					log.info(plugin.logPrefix + "Finished creating a table");
				}
				log.info(plugin.logPrefix + "Table found!");
				return true;
			}
			else{
				log.info(plugin.logPrefix
						+ "Connection to SQLite could not be established, Unloading the plugin!");
				// Disable the plugin, since there is no connection to a database
				return false;
			}
		}
		else{
			log.info(plugin.logPrefix
					+ "Connection to an SQL database could not be established, Unloading the plugin!");
			// Disable the plugin, since there is no connection to a database
			return false;
		}
	}
		
		
		//This is the old connect method replaced by the one above :)
		/*
		Logger log = plugin.logger;
		driver = new MySQL(plugin.logger, plugin.logPrefix, this.url,
				this.port, this.db, this.user, this.pass);
		try {
			driver.open();
		} catch (Exception ex) {
			log.info(plugin.logPrefix + ex.getMessage());
		}
		if (b == true) {
			if (driver.checkConnection()) {
				log.info(plugin.logPrefix + "Established connection to MySQL!");
				log.info(plugin.logPrefix + "Looking for data...");
				if (!driver.checkTable(table)) {
					log.info(plugin.logPrefix
							+ "No data found, creating table now!");
					this.createTable();
					log.info(plugin.logPrefix + "Finished creating a table");
				}
				log.info(plugin.logPrefix + "Table found!");
				return true;
			}
			log.info(plugin.logPrefix
					+ "Connection to MySQL could not be established!");
			// Disable the plugin, since there is no connection to a database
			plugin.getServer().getPluginManager().disablePlugin(plugin);
			return false;
		} else {
			if (driver.checkConnection()) {
				driver.close();
			}
			return false;
		}
		*/

	private void createTable(MySQL mysqldriver) {
		mysqldriver.createTable("CREATE TABLE kblb ( PlayerName Varchar(255), Kills int, Deaths int, Ratio double(4,2))");
		return;
	}
	
	private void createTable(SQLite sqlitedriver){
		sqlitedriver.createTable("CREATE TABLE kblb ( PlayerName Varchar(255), Kills int, Deaths int, Ratio double(4,2))");
		return;
	}

	@SuppressWarnings("unused")
	public int getKills(String playername) throws SQLException {
		ResultSet set = null;
		try {
			set = driver.query("SELECT * FROM kblb WHERE PlayerName='"
					+ playername.toLowerCase() + "'");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 70 Class : SQL.java");
		}
		set.close();
		if (set != null) {
			return set.getInt(2);
		}
		return -1;
	}

	@SuppressWarnings("unused")
	public int getDeaths(String playername) throws SQLException {
		ResultSet set = null;
		try {
			set = driver.query("SELECT * FROM kblb WHERE PlayerName='"
					+ playername.toLowerCase() + "'");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 85 Class : SQL.java");
		}
		set.close();
		if (set != null) {
			return set.getInt(3);
		}
		return -1;
	}

	public double calculateKDR(int kills, int deaths) {
		if (deaths == 0 && kills == 0) {
			return 0;
		} else if (kills > 0 && deaths == 0) {
			return kills;
		}
		return new Double(kills / deaths);
	}

	public double calculateKDR(String pname) throws SQLException {
		int kills = this.getKills(pname);
		int deaths = this.getDeaths(pname);
		if (deaths == 0 && kills == 0) {
			return 0;
		} else if (kills > 0 && deaths == 0) {
			return kills;
		}
		return new Double(kills / deaths);
	}

	public void update(String playername, int killcount, int deathcount) {
		driver.query("UPDATE kblb SET Kills='" + killcount + "', Ratio='"
				+ this.calculateKDR(killcount, deathcount) + "', Deaths='"
				+ deathcount + "' WHERE PlayerName ='"
				+ playername.toLowerCase() + "'");
	}

	public void resetAll() throws SQLException {
		ResultSet set = null;
		try {
			set = driver.query("SELECT * FROM kblb");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 125 Class : SQL.java");
		}
		if (set != null) {
			while (set.next()) {
				String pname = set.getString(1);
				this.update(pname, 0, 0);
			}
		}
	}

	public MySQL getDriver() {
		return driver;
	}

	public String[] getTopKills(int amount) throws SQLException {
		String[] map = new String[amount];
		ResultSet set = null;
		int counter = 0;
		try {
			set = driver.query("SELECT * FROM kblb ORDER BY Kills DESC");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 146 Class : SQL.java");
		}
		while (set.next()) {
			if (counter < amount) {
				String name = set.getString(1);
				map[counter] = name;
				counter++;
			}
			break;
		}
		return map;
	}

	public String[] getTopKDR(int amount) throws SQLException {
		String[] map = new String[amount];
		ResultSet set = null;
		int counter = 0;
		try {
			set = driver.query("SELECT * FROM kblb ORDER BY Ratio DESC");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 154 Class : SQL.java");
		}
		while (set.next()) {
			if (counter < amount) {
				String name = set.getString(1);
				map[counter] = name;
				counter++;
			}
			break;
		}
		return map;
	}
	
	/*

	public int getRankKills(String name) throws SQLException {
		int counter = 1;
		int rank = 0;
		boolean found = false;
		ResultSet set = null;

		try {
			set = driver.query("SELECT * FROM kblb ORDER BY Kills DESC");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 188 Class : SQL.java");
		}
		while (set.next() && !found) {
			String n = set.getString(1);

			if (n.equalsIgnoreCase(name)) {
				rank = counter;
				found = true;
			}
			counter++;
			break;
		}

		return rank;
	}
	
	//both rank methods are neat and useful good job
	public int getRankKDR(String name) throws SQLException {
		int counter = 1;
		int rank = 0;
		boolean found = false;
		ResultSet set = null;

		try {
			set = driver.query("SELECT * FROM kblb ORDER BY Ratio DESC");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 213 Class : SQL.java");
		}
		while (set.next() && !found) {
			String n = set.getString(1);

			if (n.equalsIgnoreCase(name)) {
				rank = counter;
				found = true;
			}
			counter++;
			break;
		}

		return rank;
	}
	*/
	
	
	//This methos allows multiple sorting methods at one method 
	public int getRank(String name, String sortmethod) throws SQLException{
		String sort = "Kills";
		if(sortmethod.equalsIgnoreCase("kills")){
			sort = "Kills";
		}
		else{
			sort = "Ratio";
		}
		int counter = 1;
		int rank = 0;
		boolean found = false;
		ResultSet set = null;

		try {
			set = driver.query("SELECT * FROM kblb ORDER BY " + sort + " DESC");
		} catch (Exception e) {
			plugin.logger.warning(plugin.logPrefix
					+ "Error at line 213 Class : SQL.java");
		}
		while (set.next() && !found) {
			String n = set.getString(1);

			if (n.equalsIgnoreCase(name)) {
				rank = counter;
				found = true;
			}
			counter++;
			break;
		}

		return rank;
	}
}
