package me.anni.LeaderBoards;

import java.sql.SQLException;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class LeaderBoardsPlayerListener implements Listener {

	private LeaderBoards plugin;

	public LeaderBoardsPlayerListener(LeaderBoards instance) {
		plugin = instance;
	}

	@EventHandler
	public void onEntitiyDeath(final EntityDeathEvent event) {
		if (event.getEntity() instanceof Player) {
			Player victim = (Player) event.getEntity();
			if (victim.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) victim
						.getLastDamageCause();
				if (e.getDamager() instanceof Player) {
					Player attacker = (Player) e.getDamager();
					int victimdeaths = 0;
					int victimkills = 0;
					int killerkills = 0;
					int killerdeaths = 0;

					try {
						killerkills = plugin.data.getKills(attacker.getName());
					} catch (SQLException e1) {
						plugin.logger.warning(plugin.logPrefix
								+ "Error at line 34 " + e1.getMessage());
					}
					try {
						killerdeaths = plugin.data
								.getDeaths(attacker.getName());
					} catch (SQLException e1) {
						plugin.logger.warning(plugin.logPrefix
								+ "Error at line 40 " + e1.getMessage());
					}
					try {
						victimdeaths = plugin.data.getDeaths(victim.getName());
					} catch (SQLException e1) {
						plugin.logger.warning(plugin.logPrefix
								+ "Error at line 46 " + e1.getMessage());
					}
					try {
						victimkills = plugin.data.getKills(victim.getName());
					} catch (SQLException e1) {
						plugin.logger.warning(plugin.logPrefix
								+ "Error at line 51 " + e1.getMessage());
					}
					victimdeaths++;
					killerkills++;

					plugin.data.update(victim.getName(), victimkills,
							victimdeaths);
					plugin.data.update(attacker.getName(), killerkills,
							killerdeaths);
				}
			}
		}

	}

}
